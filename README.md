# TS Node Lib Archetype
This is a typescript based node project.

## Install
Run `npm i` in the root to install.

## Build
Run `npm run build` to build the source.
Run `npm run build:dev` to build, lint and test the source.
Run `npm run build:all` to build, lint, test, cover and generate docs from jsdocs.

### Quality
Run `npm run lint` to lint the source and `npm run lint -- --fix` to enable auto-fixing.
Run `npm run test` after the project was builded to test the 'dist' folder.
Run `npm run cover` to test and check code coverage, after you could run `npm run cover:percentage` to get total lines coverage percentage.
Run `npm run docs` to generate a documentatios static webpage based on all source tsdocs.

## Version
Run `npm run version:current` to check the current version.
Run `npm run version:patch` to add one patch version.
Run `npm run version:minor` to add one minor version.
Run `npm run version:major` to add one major version.

## Visual Studio Code
This project has a setup for visual studio code. Please install all recommended extensions, go to extensions panel and search for `@recommended` and install all 'Worspace recommendations'.

### Debug
This project also has a setup for debugging from `vscode` IDE. Just set a breakpoint in any 'test.ts' file and launch 'Mocha Tests' from 'Debug' panel.
