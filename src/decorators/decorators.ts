import { instance } from '../express/express-application';
import { PathParams, Request, Response } from 'express-serve-static-core';
import { JsonMockHandler } from './interfaces';

// tslint:disable-next-line: variable-name
export const All = (path: PathParams) => {
  return (target: any, propertyKey: string) => {
    const meta = target.__middleware_meta = target.__middleware_meta || {};
    const triggers = meta.triggers = meta.triggers || [];
    triggers.push((currentInstance) => {
      const middleware = currentInstance[propertyKey];
      console.log(`Resolviendo middleware ${propertyKey}`);
      instance.all(path, middleware.bind ? middleware.bind(currentInstance) : middleware);
    });
  };
};

// tslint:disable-next-line: variable-name
export const Get = (path: PathParams) => {
  return (target: any, propertyKey: string) => {
    const meta = target.__middleware_meta = target.__middleware_meta || {};
    const triggers = meta.triggers = meta.triggers || [];
    triggers.push((currentInstance) => {
      const middleware = currentInstance[propertyKey];
      console.log(`Resolviendo middleware ${propertyKey}`);
      instance.get(path, middleware.bind ? middleware.bind(currentInstance) : middleware);
    });
  };
};

// tslint:disable-next-line: variable-name
export const Post = (path: PathParams) => {
  return (target: any, propertyKey: string) => {
    const meta = target.__middleware_meta = target.__middleware_meta || {};
    const triggers = meta.triggers = meta.triggers || [];
    triggers.push((currentInstance) => {
      const middleware = currentInstance[propertyKey];
      console.log(`Resolviendo middleware ${propertyKey}`);
      instance.post(path, middleware.bind ? middleware.bind(currentInstance) : middleware);
    });
  };
};

// tslint:disable-next-line: variable-name
export const Put = (path: PathParams) => {
  return (target: any, propertyKey: string) => {
    const meta = target.__middleware_meta = target.__middleware_meta || {};
    const triggers = meta.triggers = meta.triggers || [];
    triggers.push((currentInstance) => {
      const middleware = currentInstance[propertyKey];
      console.log(`Resolviendo middleware ${propertyKey}`);
      instance.put(path, middleware.bind ? middleware.bind(currentInstance) : middleware);
    });
  };
};

// tslint:disable-next-line: variable-name
export const Delete = (path: PathParams) => {
  return (target: any, propertyKey: string) => {
    const meta = target.__middleware_meta = target.__middleware_meta || {};
    const triggers = meta.triggers = meta.triggers || [];
    triggers.push((currentInstance) => {
      const middleware = currentInstance[propertyKey];
      console.log(`Resolviendo middleware ${propertyKey}`);
      instance.delete(path, middleware.bind ? middleware.bind(currentInstance) : middleware);
    });
  };
};

// tslint:disable-next-line: variable-name
export const Use = (path?: PathParams) => {
  return (target: any, propertyKey: string) => {
    const meta = target.__middleware_meta = target.__middleware_meta || {};
    const triggers = meta.triggers = meta.triggers || [];
    triggers.push((currentInstance) => {
      const middleware = currentInstance[propertyKey];
      console.log(`Resolviendo middleware ${propertyKey}`);
      const args = [];
      if (path) {
        args.push(path);
      }
      args.push(middleware.bind ? middleware.bind(currentInstance) : middleware);
      instance.use.apply(instance, args);
    });
  };
};

// tslint:disable-next-line: variable-name
export const JsonMock = (handler?: JsonMockHandler) => {
  return (target: any, propertyKey: string) => {
    // tslint:disable-next-line: no-parameter-reassignment
    handler = handler || {};
    let json = {};

    const calculateDelay = (delay) => {
      if (!delay) {
        return 0;
      }
      if (Number.isFinite(Number(delay))) {
        return delay;
      }
      const random = Math.floor(Math.random() * delay.max);
      const min = (delay.min || 0);
      if (random < min) {
        return random + min;
      }
      return random;
    };

    Object.defineProperty(target, propertyKey, {
      get: () => {
        return (req: Request, res: Response): any => {
          res.set(handler.headers ? ((handler.headers instanceof Function) ? handler.headers(req) : handler.headers) : {});
          res.status(handler.status || 200);
          res.statusMessage =
            handler.statusText ? ((handler.statusText instanceof Function) ? handler.statusText(req) : handler.statusText) : '';

          setTimeout(
            () => {
              res.json(json);
            },
            calculateDelay(handler.delay));
        };
      },
      set: (jsonVal) => {
        json = jsonVal;
      },
    });
  };
};
