import { Request } from 'express';

export interface Headers {
  [key: string]: string;
}

export interface JsonMockHandler {
  delay?: number | { min?: number, max: number };
  headers?: (Headers | ((request: Request) => Headers));
  status?: number;
  statusText?: (string | ((request: Request) => string));
}
// json: (T | Array<T> | number | ((request : Request)=> T | Array<T> | number));
