import { AuthService } from './auth.service';

/**
 * Acá ponemos cada clase de servicio que quiero usar como middleware.
 * Considerar que se van a instanciar en este orden.
 */
export const SERVICES = [
  AuthService,
];
