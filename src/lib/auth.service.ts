import { Post, Put, Use } from '../decorators';
import { Response, Request } from 'express';
import express = require('express');

export class AuthService {

  @Use()
  json = express.json();

  // private usersDB = [
  //   { username: 'admin', password: 'admin' },
  // ];

  /**
   * Servicio que debe autentificar a un usuario
   * si el usuario exite en usersDB, debe responder 200,
   * caso contrario debe responder 401
   * @param req el resquest
   * @param res el response
   */
  @Post('/auth/login')
  doLogin(req: Request, res: Response) {
    // const userInfo: { username: string, password: string } = req.body;
    setTimeout(
      () => {
        res.status(404).send();
      },
      1000);
  }

  /**
   * Servicio que debe crear un usuario
   * si el usuario exite en usersDB, debe responder 409,
   * si no existe y el body tiene username y password correctamente,
   * lo agrega a usersDB y responde 201
   * en cualquier otro caso responde 400
   * @param req el resquest
   * @param res el response
   */
  @Put('/auth/register')
  doRegister(req: Request, res: Response) {
    // const userInfo: { username: string, password: string } = req.body;
    setTimeout(
      () => {
        res.status(404).send();
      },
      1000);
  }

}
