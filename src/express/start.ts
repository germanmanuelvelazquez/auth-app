import { instance } from '../express/express-application';
import { SERVICES } from '../lib';

console.log('Inicializando servicios...');
SERVICES
  .forEach((klazz) => {
    const currentInstance = new klazz();
    const meta = currentInstance['__middleware_meta'] || { triggers: [] };
    meta.triggers.forEach((trigger) => {
      trigger(currentInstance);
    });
  });

const PORT = 3000;
const HOST = '127.0.0.1';

console.log(`Escuchando en http://${HOST}:${PORT}`);
instance.listen(PORT, HOST);
